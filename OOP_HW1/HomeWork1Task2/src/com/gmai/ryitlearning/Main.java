package com.gmai.ryitlearning;

public class Main {

	public static void main(String[] args) {
		/**
		 * 2) ������� ����� �Triangle�. � �������� ������� �������� ����� ������
		 * ������������. ���������� �����, ������� ����� ���������� ������� �����
		 * ������������. �������� ��������� �������� ����� ������ � ������������� ��.
		 */

		Triangle triangle1 = new Triangle();
		triangle1.setSideA(3.5);
		triangle1.setSideB(4.2);
		triangle1.setSideC(7.2);

		System.out.println(triangle1);

		String s1 = String.format("The area of the 1st triangle = " + "%.2f", triangle1.area());
		System.out.println(s1);

		Triangle triangle2 = new Triangle(13, 14, 6);

		System.out.println(triangle2);

		String s2 = String.format("The area of the 2nd triangle = " + "%.2f", triangle2.area());
		System.out.println(s2);
		
		Triangle triangle3 = new Triangle(23.9, 17.1, 6.85);

		System.out.println(triangle3);

		String s3 = String.format("The area of the 3rd triangle = " + "%.2f", triangle3.area());
		System.out.println(s3);
	}

}

import java.util.Scanner;

public class Cat {

	private String name;
	private String color;
	private String breed;
	private int age;
	private double weight;

	public Cat(String name, String color, String breed, int age, double weight) {
		super();
		this.name = name;
		this.color = color;
		this.breed = breed;
		this.age = age;
		this.weight = weight;
	}

	public Cat() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	

	public String scratchCouch(String scratch) {

		String response;

		if (scratch == "Yes") {
			response = "You need a new couch";
		} else if (scratch == "No") {
			response = "Such a good little kitty you have";

		} else {
			response = "Invalid input";
		}
		return response;
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + ", color=" + color + ", breed=" + breed + ", age=" + age + ", weight=" + weight
				+ "]";
	}

}

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/**
		 * 1) ������� ����� �Cat� (� �������� ������� ����� ����� ��������� �������).
		 * �������� ��� ���������� � ��������. ������� ��������� ����������� ��������
		 * ����� ������. ������������ ��� �������.
		 * 
		 */

		Cat cat1 = new Cat();
		cat1.setName("Vasya");
		cat1.setColor("Red and White");
		cat1.setBreed("Nobody knows");
		cat1.setAge(7);
		cat1.setWeight(8.5);

		Cat cat2 = new Cat("Kuzya", "Black&White", "MaineKune", 8, 12);

		Cat cat3 = new Cat("Murchik", "Blue", "British", 2, 5.4);

		System.out.println(cat1);
		System.out.println(cat2);
		System.out.println(cat3);

		System.out.println(cat1.scratchCouch("Yes"));
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Is " + cat2.getName() + " scratching your couch?");
		System.out.println(cat2.scratchCouch(sc.nextLine()));
		sc.close();

	}

}

/**												HW3_OOp 
 * 												 Task_3
 *  3)  ��������  �����  ������,  �������  ��������  ������  ��  10 ��������  ������  �������.  ����������  ������  ����������, 
		 * ��������  ��������  �  �����  ������  ��������  ��  �������.  � ������    �������  ����������  11  ��������,  �������� 
		 * �����������  ����������  �  �����������  ���.
 * 
 */

package com.gmail.ryitlearning;

import java.util.Scanner;

import com.gmail.ryitlearning.exception.MyException;

public class GroupOfStudents implements Voencom {

	private Student[] group = new Student[10];
	private String groupName;

	public GroupOfStudents(Student[] group, String groupName) {
		super();
		this.group = group;
		this.groupName = groupName;
	}

	public GroupOfStudents() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student[] getGroup() {
		return group;
	}

	public void setGroup(Student[] group) {
		this.group = group;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void addStudent(Student st1) throws MyException {

		if (st1 == null) {
			throw new IllegalArgumentException("Null student");
		}

		for (int i = 0; i <= group.length; i++) {

			if (group[i] == null) {

				group[i] = st1;
				return;
			}
		}
		throw new MyException();

	}


	public Student findStudent(String lastName) {
		for (Student st1 : group) {
			if (st1 != null && lastName.equalsIgnoreCase(st1.getLastName())) {
				return st1;
			}
		}
		return null;
	}

	public boolean deleteStudent(long Number) {
		for (int i = 0; i < group.length; i++) {
			if (group[i] != null && group[i].getStudentBook() != 0) {
				group[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public void interactAddStudent(Student std) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("To add student follow system requests.");
		
		System.out.println("Input First Name and press Enter");
		std.setFirstName(sc.nextLine());
		System.out.println("Input Last Name and press Enter");
		std.setLastName(sc.nextLine());
		System.out.println("Input sex and press Enter (Male/fMale)");
		std.setSex(sc.nextLine());
		System.out.println("Input Year of Birth (19xx) and press Enter");
		std.setAge(sc.nextInt());
		System.out.println("Input Course (1-5)");
		std.setCourse(sc.nextInt());
		System.out.println("Input Student book number (3 digit) and press Enter");
		std.setStudentBook(sc.nextInt());
		System.out.println("Input Student average mark (1-10) and press Enter");
		std.setAverageMark(sc.nextDouble());	
		
		sc.close();
		for (int i = 0; i < group.length; i++) {
			if( std == null) {
				group[i] = std;
				return;
			}	
		}
	}
		

	@Override
	public String toString() {
		return groupName + ":" + "\n" + 
								 "1. " + group[0] + ";" + "\n" + 
								 "2. " + group[1] + ";" + "\n" +
								 "3. " + group[2] + ";" + "\n" + 
								 "4. " + group[3] + ";" + "\n" + 
								 "5. " + group[4] + ";" + "\n" + 
								 "6. " + group[5] + ";" + "\n" + 
								 "7. " + group[6] + ";" + "\n" + 
								 "8. " + group[7] + ";" + "\n" + 
								 "9. " + group[8] + ";" + "\n" + 
								"10."  + group[9] + ";" + "\n";
	}

	@Override
	public Student[] getRecruiter() {
		int n = 0;
		for (Student std : group) {
			if (std != null && std.getSex().equalsIgnoreCase("Male") && (2020 - std.getAge()) >= 18) {
				 n += 1;
			}
		}
		Student[] recruiterArray = new Student[n];
		int i = 0;
		for (Student std : group) {
			if (std != null && std.getSex().equalsIgnoreCase("Male") && (2020 - std.getAge()) >= 18) {
				recruiterArray[i] = std;
				i += 1;
			}
		}
		return recruiterArray;
	}


}

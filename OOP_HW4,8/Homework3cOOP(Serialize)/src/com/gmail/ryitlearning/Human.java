/** 											HW3_OOp 
 * 												 Task_1
 * 
 * 1)  ��������  �����,  �����������  ��������  (��������  �����, ��������� ���������� � ��������).
 */

package com.gmail.ryitlearning;

import java.io.Serializable;

public class Human implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String sex;
	private int age; // year of birth - 19xx;

	public Human(String firstName, String lastName, String sex, int age) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.age = age;
	}

	public Human() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Human [firstName=" + firstName + ", lastName=" + lastName + ", sex=" + sex + ", age=" + age + "]";
	}

	

}

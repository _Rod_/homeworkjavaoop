/**												HW3_OOp 
 * 												 Task_2
 *  2)  �� ������ ������ Human ��������  �����  �������  (�������������� ����� ������ ����������).
 * 
 */

package com.gmail.ryitlearning;

public class Student extends Human {

	private int course;
	private int studentBook;

	public Student(String firstName, String lastName, String sex, int age, int course, int studentBook) {
		super(firstName, lastName, sex, age);
		this.course = course;
		this.studentBook = studentBook;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCourse() {
		return course;
	}

	public void setCourse(int course) {
		this.course = course;
	}

	public int getStudentBook() {
		return studentBook;
	}

	public void setStudentBook(int studentBook) {
		this.studentBook = studentBook;
	}

	@Override
	public String toString() {
		return super.getFirstName() + " " + super.getLastName() + "; " + "course=" + course + "; studentBook="
				+ studentBook + "; " + "sex = " + super.getSex() + "; age" + super.getAge() + "]";
	}

}

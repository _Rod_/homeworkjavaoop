package com.gmail.ryitlearning;

import com.gmail.ryitlearning.exception.MyException;

public class Main {

	public static void main(String[] args) throws MyException {
		/**
		 * 1) �������� �����, ����������� �������� (�������� �����, ��������� ����������
		 * � ��������).
		 * 
		 * 2) �� ��� ������ �������� ����� ������� (�������������� ����� ������
		 * ����������).
		 * 
		 * 3) �������� ����� ������, ������� �������� ������ �� 10 �������� ������
		 * �������. ���������� ������ ����������, �������� �������� � ����� ������
		 * �������� �� �������. � ������ ������� ���������� 11 ��������, ��������
		 * ����������� ���������� � ����������� ���.
		 */

		GroupOfStudents group1 = new GroupOfStudents();

		group1.setGroupName("Group#1");

		Student st1 = new Student("Vladimir", "Shevchenko", "Male", 1992, 1, 563);
		Student st2 = new Student("Anna", "Nikiforova", "FMale", 1989, 2, 896);
		Student st3 = new Student("Nikifor", "Morozov", "Male", 1988, 2, 888);
		Student st4 = new Student("Sergey", "Popov", "Male", 1990, 2, 890);
		Student st5 = new Student("Anton", "Solodov", "Male", 1987, 2, 745);
		Student st6 = new Student("Grigoriy", "Anotonov", "Male", 1990, 2, 436);
		Student st7 = new Student("Dariya", "Petrova", "FMale", 1989, 2, 877);
		Student st8 = new Student("Alisa", "Seleznyova", "FMale", 1991, 2, 721);
		Student st9 = new Student("Yuriy", "Gagarin", "Male", 1961, 1, 001);
		Student st10 = new Student("Fedor", "Varenik", "Male", 1996, 2, 590);
		Student st11 = new Student("Anfisa", "Popudrenko", "FMale", 1988, 1, 540);

		group1.addStudent(st1);
		group1.addStudent(st2);
		group1.addStudent(st3);
		group1.addStudent(st4);
		group1.addStudent(st5);
		group1.addStudent(st6);
		group1.addStudent(st7);
		group1.addStudent(st8);
		group1.addStudent(st9);
		group1.addStudent(st10);

		System.out.println(group1.toString());
		
		/**
		group1.addStudent(st11);
		System.out.println(group1.toString());
		*/

		group1.deleteStudent(0);
		group1.deleteStudent(4);

		System.out.println(group1.toString());

		System.out.println(group1.findStudent("Gagarin"));
		System.out.println(group1.findStudent("Varenik"));

	}

}

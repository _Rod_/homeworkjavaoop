/**												HW3_OOp 
 * 												 Task_3
 *  3)  ��������  �����  ������,  �������  ��������  ������  ��  10 ��������  ������  �������.  ����������  ������  ����������, 
		 * ��������  ��������  �  �����  ������  ��������  ��  �������.  � ������    �������  ����������  11  ��������,  �������� 
		 * �����������  ����������  �  �����������  ���.
 * 
 */

package com.gmail.ryitlearning;

import java.util.Arrays;

import com.gmail.ryitlearning.exception.MyException;

public class GroupOfStudents {

	private Student[] group = new Student[10];
	private String groupName;

	public GroupOfStudents(Student[] group, String groupName) {
		super();
		this.group = group;
		this.groupName = groupName;
	}

	public GroupOfStudents() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student[] getGroup() {
		return group;
	}

	public void setGroup(Student[] group) {
		this.group = group;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void addStudent(Student st1) throws MyException {

		if (st1 == null) {
			throw new IllegalArgumentException("Null stuent");
		}

		for (int i = 0; i <= group.length; i++) {

			if (group[i] == null) {

				group[i] = st1;
				return;
			}
		}
		throw new MyException();

	}

	public void delStudent(Student st1) {
		for (int i = 0; i <= group.length; i++) {
			if (group[i].equals(st1)) {
				group[i] = null;
				break;
			}
		}
	}

	public Student findStudent(String lastName) {
		for (Student st1 : group) {
			if (st1 != null && lastName.equalsIgnoreCase(st1.getLastName())) {
				return st1;
			}
		}
		return null;
	}

	public boolean deleteStudent(long Number) {
		for (int i = 0; i < group.length; i++) {
			if (group[i] != null && group[i].getStudentBook() != 0) {
				group[i] = null;
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return groupName + ":" + "\n" + 
								 "1. " + group[0] + ";" + "\n" + 
								 "2. " + group[1] + ";" + "\n" +
								 "3. " + group[2] + ";" + "\n" + 
								 "4. " + group[3] + ";" + "\n" + 
								 "5. " + group[4] + ";" + "\n" + 
								 "6. " + group[5] + ";" + "\n" + 
								 "7. " + group[6] + ";" + "\n" + 
								 "8. " + group[7] + ";" + "\n" + 
								 "9. " + group[8] + ";" + "\n" + 
								 "10. " + group[9] + ";" + "\n";
	}


}

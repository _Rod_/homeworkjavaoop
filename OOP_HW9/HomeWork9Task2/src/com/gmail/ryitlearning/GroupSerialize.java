package com.gmail.ryitlearning;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class GroupSerialize implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static void saveGroupSerial(Serializable obj, File file) throws IOException {
		try	(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))){
			oos.writeObject(obj);
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		} catch (IOException e) {
			throw e;
		}
	}
	
	public static Serializable loadGroupSerial(File file) throws IOException, ClassNotFoundException {
		try (ObjectInput oi = new ObjectInputStream(new FileInputStream(file))) {
			return (Serializable) oi.readObject();
		} catch (IOException e) {
			throw e;
		}
	}

}

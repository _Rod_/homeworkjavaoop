/**												HW3_OOp 
 * 												 Task_3
 *  3)  ��������  �����  ������,  �������  ��������  ������  ��  10 ��������  ������  �������.  ����������  ������  ����������, 
		 * ��������  ��������  �  �����  ������  ��������  ��  �������.  � ������    �������  ����������  11  ��������,  �������� 
		 * �����������  ����������  �  �����������  ���.
 * 
 */

package com.gmail.ryitlearning;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.gmail.ryitlearning.exception.MyException;

public class GroupOfStudents implements Voencom, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//private Student[] group = new Student[10];
	
	private List<Student> group = new ArrayList<Student>();
	private String groupName;

	public GroupOfStudents(List<Student> group, String groupName) {
		super();
		this.group = group;
		this.groupName = groupName;
	}

	public GroupOfStudents() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<Student> getGroup() {
		return group;
	}

	public void setGroup(List<Student> group) {
		this.group = group;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

//	public void addStudent(Student st1) throws MyException {
//
//		if (st1 == null) {
//			throw new IllegalArgumentException("Null student");
//		}
//
//		for (int i = 0; i <= group.length; i++) {
//
//			if (group[i] == null) {
//
//				group[i] = st1;
//				return;
//			}
//		}
//		throw new MyException();
//
//	}

	public void addStudent(Student st1) throws MyException{
		if (group.size() < 10) {
		group.add(st1);
		}
	throw new MyException();
	}

//	public Student findStudent(String lastName) {
//		for (Student st1 : group) {
//			if (st1 != null && lastName.equalsIgnoreCase(st1.getLastName())) {
//				return st1;
//			}
//		}
//		return null;
//	}
	
	public Student findStudent(String lastName) {
		for(Student st1: group) {
			if(st1.getLastName().equals(lastName)) {
				return st1;
			}
		}
		return null;
	}

//	public boolean deleteStudent(long Number) {
//		for (int i = 0; i < group.length; i++) {
//			if (group[i] != null && group[i].getStudentBook() != 0) {
//				group[i] = null;
//				return true;
//			}
//		}
//		return false;
//	}
	
	public boolean deleteStudent(long number) {
		for(Student st1: group) {
			if(number == st1.getStudentBook()) {
				group.remove(group.indexOf(st1));
				return true;
			}		
	    }
		return false;
	}
	
//	public void interactAddStudent(Student std) {
//		
//		Scanner sc = new Scanner(System.in);
//		System.out.println();
//		System.out.println("To add student follow system requests.");
//		
//		System.out.println("Input First Name and press Enter");
//		std.setFirstName(sc.nextLine());
//		System.out.println("Input Last Name and press Enter");
//		std.setLastName(sc.nextLine());
//		System.out.println("Input sex and press Enter (Male/fMale)");
//		std.setSex(sc.nextLine());
//		System.out.println("Input Year of Birth (19xx) and press Enter");
//		std.setAge(sc.nextInt());
//		System.out.println("Input Course (1-5)");
//		std.setCourse(sc.nextInt());
//		System.out.println("Input Student book number (3 digit) and press Enter");
//		std.setStudentBook(sc.nextInt());
//		System.out.println("Input Student average mark (1-10) and press Enter");
//		std.setAverageMark(sc.nextDouble());	
//		
//		sc.close();
//		for (int i = 0; i < group.length; i++) {
//			if( std == null) {
//				group[i] = std;
//				return;
//			}	
//		}
//	}
		
	
	public void interactAddStudent(Student std) {
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("To add student follow system requests.");
		
		System.out.println("Input First Name and press Enter");
		std.setFirstName(sc.nextLine());
		System.out.println("Input Last Name and press Enter");
		std.setLastName(sc.nextLine());
		System.out.println("Input sex and press Enter (Male/fMale)");
		std.setSex(sc.nextLine());
		System.out.println("Input Year of Birth (19xx) and press Enter");
		std.setAge(sc.nextInt());
		System.out.println("Input Course (1-5)");
		std.setCourse(sc.nextInt());
		System.out.println("Input Student book number (3 digit) and press Enter");
		std.setStudentBook(sc.nextInt());
		System.out.println("Input Student average mark (1-10) and press Enter");
		std.setAverageMark(sc.nextDouble());
		sc.close();
		
		if(group.size() < 10) {
			group.add(std);
			return;
		} else System.out.println("The group is allready full");
	}

	@Override
	public String toString() {
		return groupName + ":" + "\n" + 
								 "1. " + group.get(0) + ";" + "\n" + 
								 "2. " + group.get(1) + ";" + "\n" +
								 "3. " + group.get(2) + ";" + "\n" + 
								 "4. " + group.get(3) + ";" + "\n" + 
								 "5. " + group.get(4) + ";" + "\n" + 
								 "6. " + group.get(5) + ";" + "\n" + 
								 "7. " + group.get(6) + ";" + "\n" + 
								 "8. " + group.get(7) + ";" + "\n" + 
								 "9. " + group.get(8) + ";" + "\n" + 
								"10."  + group.get(9) + ";" + "\n";
	}

	

//	@Override
//	public Student[] getRecruiter() {
//		int n = 0;
//		for (Student std : group) {
//			if (std != null && std.getSex().equalsIgnoreCase("Male") && (2020 - std.getAge()) >= 18) {
//				 n += 1;
//			}
//		}
//		Student[] recruiterArray = new Student[n];
//		int i = 0;
//		for (Student std : group) {
//			if (std != null && std.getSex().equalsIgnoreCase("Male") && (2020 - std.getAge()) >= 18) {
//				recruiterArray[i] = std;
//				i += 1;
//			}
//		}
//		return recruiterArray;
//	}
	
	@Override
	public List<Student> getRecruiter(List<Student> group) {
		List <Student> recruits = new ArrayList<Student>();
		for(Student std: group) {
			if (std.getSex().equalsIgnoreCase("Male") && (2020 - std.getAge()) >= 18) {
				recruits.add(std);
			}
		}
		return recruits;
		
	}


}

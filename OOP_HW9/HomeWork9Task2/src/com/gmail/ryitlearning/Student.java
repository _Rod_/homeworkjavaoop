/**												HW3_OOp 
 * 												 Task_2
 *  2)  �� ������ ������ Human ��������  �����  �������  (�������������� ����� ������ ����������).
 * 
 */

package com.gmail.ryitlearning;

import java.io.Serializable;

public class Student extends Human implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int course;
	private int studentBook;
	private double averageMark;

	
	public Student(String firstName, String lastName, String sex, int age, int course, int studentBook,
			double averageMark) {
		super(firstName, lastName, sex, age);
		this.course = course;
		this.studentBook = studentBook;
		this.averageMark = averageMark;
	}



	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getCourse() {
		return course;
	}

	public void setCourse(int course) {
		this.course = course;
	}

	public int getStudentBook() {
		return studentBook;
	}

	public void setStudentBook(int studentBook) {
		this.studentBook = studentBook;
	}

	

	public double getAverageMark() {
		return averageMark;
	}



	public void setAverageMark(double averageMark) {
		this.averageMark = averageMark;
	}



	@Override
	public String toString() {
		return super.getFirstName() + " " + super.getLastName() + "; " + "course = " + course
				+ "; student book number = " + studentBook + "; " + "average mark = " + averageMark + "; " + 
				"sex = " + super.getSex() + "; year of birth = " + super.getAge() + "]";
	}

}

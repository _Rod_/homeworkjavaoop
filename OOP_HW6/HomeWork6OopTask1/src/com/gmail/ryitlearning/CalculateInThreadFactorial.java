package com.gmail.ryitlearning;

import java.math.BigInteger;

public class CalculateInThreadFactorial implements Runnable {
	
	private int number;

	public CalculateInThreadFactorial(int number) {
		super();
		this.number = number;
	}
	
	
	public BigInteger factorial(int n) {
		BigInteger f = new BigInteger("1");
		for (int i = 1; i <= n; i++) {
			f = f.multiply(new BigInteger(Integer.toString(i)));
		}
		return f;
	}


	@Override
	public void run() {
		
		for (int i = 0; i < this.number; i++) {
			System.out.println("Factorial of " + Thread.currentThread().getName() + "! " + " = " + factorial(i));
		}
		
	}
	
	

}

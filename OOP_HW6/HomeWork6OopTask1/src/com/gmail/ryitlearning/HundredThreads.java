package com.gmail.ryitlearning;

public class HundredThreads {

	Thread[] threads100 = new Thread[100];

	public HundredThreads() {
		for (int i = 0; i < threads100.length; i++) {
			threads100[i] = new Thread(new CalculateInThreadFactorial(i));
		}
	}

	public void StartTread() {
		for (Thread thread : threads100) {
			thread.start();
		}
	}

}

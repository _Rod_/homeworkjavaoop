package com.gmail.ryitlearning;

public class Circle extends Shape {
	private Point point1;
	private Point point2;

	public Circle(Point point1, Point point2) {
		super();
		this.point1 = point1;
		this.point2 = point2;
	}

	public Circle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Point getPoint1() {
		return point1;
	}

	public void setPoint1(Point point1) {
		this.point1 = point1;
	}

	public Point getPoint2() {
		return point2;
	}

	public void setPoint2(Point point2) {
		this.point2 = point2;
	}

	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		double r = point1.distance(getPoint1(), getPoint2());
		return Math.PI * Math.pow(r, 2);
	}

	@Override
	public double getPerimetr() {
		// TODO Auto-generated method stub
		double r = point1.distance(getPoint1(), getPoint2());
		return Math.PI * r * 2.0;
	}

	@Override
	public String toString() {
		return "Circle [point1=" + point1 + ", point2=" + point2 + "]";
	}

}

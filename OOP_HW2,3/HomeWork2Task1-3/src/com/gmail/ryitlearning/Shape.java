package com.gmail.ryitlearning;

public abstract class Shape {
	
	public abstract double getArea();
	
	public abstract double getPerimetr();
}
